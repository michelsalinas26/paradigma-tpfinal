package entidades.personaje;

import java.io.*;
import java.util.*;
import org.json.simple.*;
import org.json.simple.parser.*;

public class Enemigo {

    private int id;
    private int vida;
    private int danio;
    private String nombre, introduccion;
    private boolean estado;
    private List<Enemigo> enemigos = new ArrayList();
    
    //Constructor
    public Enemigo(int id, int vida, int danio, String nombre, String introduccion) {
        this.id = id;
        this.vida = vida;
        this.danio = danio;
        this.nombre = nombre;
        this.introduccion = introduccion;
    }
    
    //Constructor vacio...
    public Enemigo() {
    }

    //Crear lista de enemigos
    public List<Enemigo> crearEnemigos() {
        //Parser para JSON
        JSONParser parser = new JSONParser();
        try {
            //Parsing al archivo y casteo a objeto de tipo JSON
            Object obj = parser.parse(new FileReader("Juego.json"));
            JSONObject jsonObject = (JSONObject) obj;
            //Como el JSON tiene un Sub-Array con las cosas, tengo que llegar a ese array
            JSONArray enemigosJSON = (JSONArray) jsonObject.get("Enemigos");

            //Casteo el array para transformarlo en objeto y con leerEnemigos voy agregando los objetos que devuelve el metodo
            for (Object o : enemigosJSON) {
                enemigos.add(leerEnemigos((JSONObject) o));
            }
            //Excepciones
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo");
            e.getMessage();
        } catch (IOException e) {
            System.out.println("Excepcion de IO");
            e.getMessage();
        } catch (ParseException e) {
            System.out.println("Excepcion de PARSING");
            e.getMessage();
        }

        //Devuelvo la lista de enemigos...
        return enemigos;
    }

    //Para leer un objeto de tipo JSON con los datos del enemigo e ir devolviendo objetos
    public static Enemigo leerEnemigos(JSONObject obj) {
        int id = (int) (long) obj.get("id");
        int vida = (int) (long) obj.get("vida");
        int danio = (int) (long) obj.get("danio");
        String nombre = (String) obj.get("nombre");
        String introduccion = (String) obj.get("introduccion");
        return new Enemigo(id, vida, danio, nombre, introduccion);
    }

    public void atacar(Personaje personaje, List<Enemigo> enemigos, int cualEnemigo) {
        System.out.println(enemigos.get(cualEnemigo).getNombre() + " daño por " + enemigos.get(cualEnemigo).getDanio() + " a " + personaje.getTipoPersonaje());
        personaje.setVida(personaje.vida - enemigos.get(cualEnemigo).getDanio());
    }

    /* ========================== GETTERS Y SETTERS ===============================*/
    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getVida() {
        return this.vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public String getNombre() {
        return this.nombre;
    }

    public int getId() {
        return id;
    }

    public String getIntroduccion() {
        return introduccion;
    }

    public int getDanio() {
        return danio;
    }

    public void setDanio(int danio) {
        this.danio = danio;
    }
    
    //ToString
     @Override
    public String toString() {
        return "Enemigo{" + "id=" + id + ", nombre=" + nombre + ", introduccion=" + introduccion + '}';
    }
}
