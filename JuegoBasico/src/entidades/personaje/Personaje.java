package entidades.personaje;

import java.util.Scanner;
import sistemas.Entrada;

public abstract class Personaje {

    protected int vida;
    protected int energia;
    protected int poder;
    protected String tipoPersonaje;
    protected String hechizoSeleccionado;
    protected int pocionVida = 4;
    protected int pocionMana = 4;
    protected final int pocionEficaciaVida = 25;
    protected final int pocionEficaciaMana = 50;
    protected int danioBasico = (int) (Math.random() * 10) + 5;
    protected String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    Entrada errores = new Entrada();
    Scanner in = new Scanner(System.in);

    public Personaje(int vida, int energia, int poder, String tipoPersonaje, String tipo) {
        this.vida = vida;
        this.energia = energia;
        this.poder = poder;
        this.tipoPersonaje = tipoPersonaje;
    }

    public String getTipoPersonaje() {
        return tipoPersonaje;
    }

    public void setTipoPersonaje(String tipoPersonaje) {
        this.tipoPersonaje = tipoPersonaje;
    }

    public Personaje() {

    }

    public String elegirHechizo() {
        hechizoSeleccionado = " ";
        int opcion = 0;
        opcion = errores.entrada(in, opcion);

        if (opcion == 1 && poder >= 20) {
            hechizoSeleccionado = "opcion 1";
        } else if (opcion == 2 && poder >= 50) {
            hechizoSeleccionado = "opcion 2";
        } else if (opcion == 3 && poder >= 100) {
            hechizoSeleccionado = "opcion 3";
        } else if (opcion == 4) {
            hechizoSeleccionado = "opcion 4";
        } else {
            hechizoSeleccionado = "opcion 5";
        }

        return hechizoSeleccionado;

    }

    public void menuCombate(Enemigo enemigo) {
        infoBatalla();
        System.out.println("Selecciona una opción: 1- [Ataque básico]  2- [Lanzar hechizos]  3-[Usar pociones]");

        int opcion = 0;
        opcion = errores.entrada(in, opcion);

        if (opcion == 1) {

            System.out.println(tipoPersonaje + " ha lanzado un golpe básico y realizó " + danioBasico + " de dano");
            int danio = enemigo.getVida() - danioBasico;
            System.out.println(tipo);
            if (tipo.equals("guerrero") && poder < 100) {
                poder += 25;
            } else if (tipo.equals("mago")){
                poder += 5;
            }

            enemigo.setVida(danio);
            System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
            System.out.println("==================================================================================");

        } else if (opcion == 2) {
            menuHechizo(enemigo);
            lanzarHechizo(enemigo);

        } else if (opcion == 3) {
            usarPocion(enemigo);
        }
    }

    public abstract void usarPocion(Enemigo enemigo);

    public abstract void lanzarHechizo(Enemigo enemigo);

    public abstract void menuHechizo(Enemigo enemigo);

    public abstract void infoBatalla();
    
    
    public void setVida(int vida) {
        this.vida = vida;
    }

    /* ============================ GETTERS Y SETTERS ============================ */
    public int getVida() {
        return vida;
    }

    public void setEnergia(int energia) {
        this.energia = energia;
    }

    public int getEnergia() {
        return this.energia;
    }

    public void setPoder(int poder) {
        this.poder = poder;
    }

    public int getPoder() {
        return poder;
    }

}
