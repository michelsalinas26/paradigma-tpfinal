package entidades.personaje;

public class Guerrero extends Personaje {

    private int danioRealizado;
    private int danioBasico = (int) (Math.random() * 15) + 10;

    public Guerrero(String nombre) {
        this.tipoPersonaje = nombre;
        this.vida = 200;
        this.energia = 50;
    }

    public Guerrero(int vida, int energia, int poder, String tipoPersonaje, String tipo) {
        super(vida, energia, poder, tipoPersonaje, tipo);
        this.poder = poder;

    }

    @Override
    public void infoBatalla() {
        System.out.println("Información de " + tipoPersonaje + " [" + "Salud: " + vida + "] " + "[" + "Furia: " + poder + "]");
        System.out.println("================================================================================");
    }

    @Override
    public void menuHechizo(Enemigo enemigo) {
        System.out.println("Selecciona un hechizo: 1- [ Golpe potenciado | 20 Furia ] 2- [ Golpe letal | 50 Furia ]  3- [ Patada voladora | 100 Furia ]  4- [ Salir ]");
        elegirHechizo();
    }

    @Override
    public void lanzarHechizo(Enemigo enemigo) {
        switch (hechizoSeleccionado) {
            case "opcion 1":
                danioRealizado = (int) (Math.random() * 10) + 15;
                System.out.println(tipoPersonaje + " Ha realizado un golpe potenciado y realizó " + danioRealizado + " de daño.");
                int danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                poder -= 20;
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                System.out.println("==================================================================================");

                break;

            case "opcion 2":
                danioRealizado = (int) (Math.random() * 10) + 25;
                System.out.println(tipoPersonaje + " Ha realizado un golpe letal por un total de  " + danioRealizado + " de daño.");
                danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                poder -= 50;
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                System.out.println("==================================================================================");
                break;

            case "opcion 3":
                danioRealizado = (int) (Math.random() * 10) + 40;
                System.out.println(tipoPersonaje + " Ha realizado una patada voladora por un total de  " + danioRealizado + " de daño.");
                danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                poder -= 100;
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                System.out.println("==================================================================================");
                break;

            case "opcion 4":
                menuCombate(enemigo);
                break;

            case "opcion 5":
                System.out.println("Acción invalida!");
                System.out.println("==================================================================================");
                break;
        }
    }

    @Override
    public void usarPocion(Enemigo enemigo) {
        System.out.println("Selecciona una opción: 1- [Poción de vida " + "(" + pocionVida + ")" + " | +25HP ]  2-[Volver]");
        int opcion = 0;
        opcion = errores.entrada(in, opcion);

        if (opcion == 1 && pocionVida > 0) {
            vida += pocionEficaciaVida;
            pocionVida--;
            System.out.println("Haz utilizado una poción de vida y la salud aumenta a: [" + vida + "]");
            System.out.println("==================================================================================");

        } else if (opcion == 2) {
            menuCombate(enemigo);

        } else if (pocionVida == 0) {
            System.out.println("No hay pociones disponibles!");
            System.out.println("==================================================================================");
            menuCombate(enemigo);
        }

    }
}
