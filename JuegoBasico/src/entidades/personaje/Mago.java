package entidades.personaje;

public class Mago extends Personaje {

    private int danioRealizado;

    public Mago(int vida, int energia, int poder, String tipoPersonaje, String tipo) {
        super(vida, energia, poder, tipoPersonaje, tipo);

    }

    public Mago(String nombre) {
        this.tipoPersonaje = nombre;
        this.vida = 150;
        this.energia = 25;
        this.poder = 200;
    }

    @Override
    public void infoBatalla() {
        System.out.println("Información de " + tipoPersonaje + " [" + "Salud: " + vida + "] " + "[" + "Maná: " + poder + "]");
        System.out.println("================================================================================");
    }

    @Override
    public void menuHechizo(Enemigo enemigo) {
        System.out.println("Selecciona un hechizo: 1- [ Bola de fuego | 20MP ] 2- [ Meteorito | 50MP ] 3- [ Ráfaga de fuego | 100MP ]  4- [ Salir ]");
        elegirHechizo();
    }

    @Override
    public void lanzarHechizo(Enemigo enemigo) {
        switch (hechizoSeleccionado) {
            case "opcion 1":
                danioRealizado = (int) (Math.random() * 20) + 25; // random entre 25 a 45
                this.poder -= 20;
                System.out.println(tipoPersonaje + "ha lanzado el hechizo Bola de fuego y realizó " + danioRealizado + " de daño.");
                int danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                if (poder >= 0) {
                    System.out.println("El maná actual es de: " + this.poder);
                }
                System.out.println("==================================================================================");
                break;

            case "opcion 2":
                danioRealizado = (int) (Math.random() * 15) + 40; // 40 a 45
                this.poder -= 50;
                System.out.println(tipoPersonaje + " ha lanzado el hechizo Meteorito y realizó " + danioRealizado + " de daño.");
                danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                if (poder >= 0) {
                    System.out.println("El maná actual es de: " + this.poder);
                }
                System.out.println("==================================================================================");
                break;

            case "opcion 3":
                danioRealizado = (int) (Math.random() * 10) + 50; // 50 a 60
                this.poder -= 100;
                System.out.println(tipoPersonaje + " ha lanzado el Ráfaga de fuego y realizó " + danioRealizado + " de daño.");
                danio = enemigo.getVida() - danioRealizado;
                enemigo.setVida(danio);
                System.out.println("La salud de " + enemigo.getNombre() + " es: " + enemigo.getVida());
                if (poder >= 0) {
                    System.out.println("El maná actual es de: " + this.poder);
                }
                System.out.println("==================================================================================");
                break;

            case "opcion 4":
                menuCombate(enemigo);
                break;

            case "opcion 5":
                System.out.println("Maná insuficiente, no se pudo realizar esa acción");
                System.out.println("==================================================================================");
        }
    }

    @Override
    public void usarPocion(Enemigo enemigo) {
        System.out.println("Selecciona una opción: 1- [Poción de vida " + "(" + pocionVida + ")" + " | +25HP ]  2- [Poción de maná " + "(" + pocionMana + ")" + " | + 50MP]  3-[Volver]");
        int opcion = 0;
        opcion = errores.entrada(in, opcion);

        if (opcion == 1 && pocionVida > 0) {
            vida += pocionEficaciaVida;
            pocionVida--;
            System.out.println("Haz utilizado una poción de vida y la salud aumenta a: [" + vida + "]");
            System.out.println("==================================================================================");

        } else if (opcion == 2 && pocionMana > 0) {
            poder += pocionEficaciaMana;
            pocionMana--;
            System.out.println("Haz utilizado una poción de maná y la maná aumenta a: [" + poder + "]");
            System.out.println("==================================================================================");

        } else if (opcion == 3) {
            menuCombate(enemigo);

        } else if (pocionMana == 0 || pocionVida == 0) {
            System.out.println("No hay pociones disponibles!");
            System.out.println("==================================================================================");
            menuCombate(enemigo);
        }

    }

}
