package entidades.lugares;

import org.json.simple.parser.*;
import java.io.*;
import java.util.*;
import org.json.simple.*;
import sistemas.Juego;

public class Lugar extends Juego {

    private int id, haciaElNorte, haciaElSur, haciaElEste, haciaElOeste;
    private String introduccion, introduccionNorte, introduccionSur, introduccionEste, introduccionOeste;

    HashMap<Integer, Lugar> lugares = new HashMap<Integer, Lugar>();

    //Constructor vacio...
    public Lugar() {

    }

    //Constructor para los lugares, se que es medio largo pero intente muchas cosas y esta me parecia la mas simple de codear, hasta una lista de arrays intente pero no se me acomodaban los atributos y terminaba todo mezclado
    public Lugar(int id, int haciaElNorte, int haciaElSur, int haciaElEste, int haciaElOeste, String introducciones, String introduccionNorte, String introduccionSur, String introduccionEste, String introduccionOeste) {
        this.id = id;
        this.haciaElNorte = haciaElNorte;
        this.haciaElSur = haciaElSur;
        this.haciaElEste = haciaElEste;
        this.haciaElOeste = haciaElOeste;
        this.introduccion = introducciones;
        this.introduccionNorte = introduccionNorte;
        this.introduccionSur = introduccionSur;
        this.introduccionEste = introduccionEste;
        this.introduccionOeste = introduccionOeste;
    }

    public HashMap<Integer, Lugar> cargarLugares() {
        //Parser para JSON
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader("Juego.json"));
            JSONObject jsonObject = (JSONObject) obj;
            //Para obtener el array "Lugares" del JSON
            JSONArray lugaresJson = (JSONArray) jsonObject.get("Lugares");
            //Carga de lugares al mapa
            for (Object o : lugaresJson) {
                JSONObject lugarsitos = (JSONObject) o;
                lugares.put((int) (long) lugarsitos.get("id"), leerLugares(lugarsitos));
            }
            //Excepciones
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo");
            e.getMessage();
        } catch (IOException e) {
            System.out.println("Excepcion de IO");
            e.getMessage();
        } catch (ParseException e) {
            System.out.println("Excepcion de PARSING");
            e.getMessage();
        }

        //Devuelvo el mapa de lugares...
        return lugares;
    }

    //Lee los lugares en el objeto de tipo JSON que le paso
    public Lugar leerLugares(JSONObject obj) {
        int id = (int) (long) obj.get("id");
        int norte = (int) (long) obj.get("haciaElNorte");
        int sur = (int) (long) obj.get("haciaElSur");
        int este = (int) (long) obj.get("haciaElEste");
        int oeste = (int) (long) obj.get("haciaElOeste");
        String intro = (String) obj.get("introduccion");
        String introN = (String) obj.get("introduccionNorte");
        String introS = (String) obj.get("introduccionSur");
        String introE = (String) obj.get("introduccionEste");
        String introO = (String) obj.get("introduccionOeste");
        return new Lugar(id, norte, sur, este, oeste, intro, introN, introS, introE, introO);
    }

    public void mostrarLugar(int codigoAMostrar, int aDondeEstoy, HashMap<Integer, Lugar> mapito) {

        haciaElNorte(aDondeEstoy, mapito);
        haciaElSur(aDondeEstoy, mapito);
        haciaElEste(aDondeEstoy, mapito);
        haciaElOeste(aDondeEstoy, mapito);
    }

    public void haciaElNorte(int lugarActual, HashMap<Integer, Lugar> mapito) {
        if (mapito.get(lugarActual).haciaElNorte != 0) {
            System.out.println("Opcion 1 - Hacia el norte : " + mapito.get(lugarActual).introduccionNorte);
        }
    }

    public void haciaElSur(int lugarActual, HashMap<Integer, Lugar> mapito) {
        if (mapito.get(lugarActual).haciaElSur != 0) {
            System.out.println("Opcion 2 - Hacia el sur : " + mapito.get(lugarActual).introduccionSur);
        }
    }

    public void haciaElEste(int lugarActual, HashMap<Integer, Lugar> mapito) {
        if (mapito.get(lugarActual).haciaElEste != 0) {
            System.out.println("Opcion 3 - Hacia el este : " + mapito.get(lugarActual).introduccionEste);
        }
    }

    public void haciaElOeste(int lugarActual, HashMap<Integer, Lugar> mapito) {
        if (mapito.get(lugarActual).haciaElOeste != 0) {
            System.out.println("Opcion 4 - Hacia el oeste : " + mapito.get(lugarActual).introduccionOeste);
        }
    }

    //GETTERS Y SETTERS NECESARIOS
    public int getHaciaElNorte() {
        return haciaElNorte;
    }

    public int getHaciaElSur() {
        return haciaElSur;
    }

    public int getHaciaElEste() {
        return haciaElEste;
    }

    public int getHaciaElOeste() {
        return haciaElOeste;
    }

    public String getIntroduccion() {
        return introduccion;
    }

    public String getIntroduccionNorte() {
        return introduccionNorte;
    }

    public String getIntroduccionSur() {
        return introduccionSur;
    }

    public String getIntroduccionEste() {
        return introduccionEste;
    }

    public String getIntroduccionOeste() {
        return introduccionOeste;
    }

    public HashMap<Integer, Lugar> getLugares() {
        return lugares;
    }

    //EQUALS / HASHCODE / TOSTRING
    @Override
    public int hashCode() {

        return this.id;
    }

    @Override
    public boolean equals(Object lug) {
        Lugar p = (Lugar) lug;
        return this.id == p.id;

    }

    @Override
    public String toString() {
        return introduccion;
    }
}
