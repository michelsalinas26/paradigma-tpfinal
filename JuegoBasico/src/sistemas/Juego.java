package sistemas;

import entidades.lugares.Lugar;
import entidades.personaje.*;
import java.util.*;

public class Juego {

    //Variables iniciales
    protected int opcion = 0;
    protected int aDondeVoy = 0;
    protected int dondeEstoy = 1;

    //Esto es el juego...
    /*===================================================================================================================================================================================================================*/
    public void Jugar() {
        //Creo lo necesario para Jugar
        Lugar lugar = new Lugar();
        HashMap<Integer, Lugar> lugares = lugar.cargarLugares();
        Enemigo enemigo = new Enemigo();
        Entrada errores = new Entrada();
        List<Enemigo> enemigos = enemigo.crearEnemigos();
        int totalEnemigos = enemigos.size() - 3;
        Scanner mover = new Scanner(System.in);
        //Presentacion del Juego
        System.out.println("=======================================================================");
        System.out.println("Bienvenido a Syntax Error //// El RPG Clasico");
        System.out.println("Presione una tecla para continuar");
        System.out.println("=======================================================================");
        mover.nextLine();
        //Presione una tecla para continuar

        //Creacion del Personaje
        Personaje personaje = crearPersonaje(mover, errores);

        //El juego...
        do {
            //Esto me imprime donde esta el personaje actualmente...
            System.out.println("Ahora estamos en " + lugares.get(dondeEstoy).toString() + " ¿hacia donde vamos " + personaje.getTipoPersonaje() + " ?");
            
            //COMBATE
            //Si hay enemigo combate, si no hay, imprime "No hay moros en la costa"
            if (enemigos.get(dondeEstoy).getVida() > 0) {
                System.out.println("Narrador: ESPERA, UN ENEMIGO! QUE HACEMOS " + personaje.getTipoPersonaje());
                System.out.println("Opcion 1: Enfrentar ================= Opcion 2: Correr (Una vez empezado el combate no se puede salir)");
                batalla(mover, errores, enemigos, totalEnemigos, personaje, lugares);
            } else {
                System.out.println("No hay moros en la costa");
            }
            
            //MOVERSE
            movimientos(mover, lugares, totalEnemigos, personaje);
            
        } while (personaje.getEnergia() > 0 && totalEnemigos != 0);
        //Final del juego en caso de que algunas de las condiciones del while anterior dejen de ser true
        finalJuego(totalEnemigos, personaje, enemigos, enemigo);
    }

    /* Metodos para jugar */
    /* ============================================================================================================================================================ */
    /* ====================================================== CREACION DEL PERSONAJE Y INTRODUCCION AL MISMO ====================================================== */
    /* ============================================================================================================================================================ */
    public Personaje crearPersonaje(Scanner mover, Entrada errores) {
        Personaje personaje = null;
        //Aca la creacion del personaje principal...
        System.out.println("Primero vamos a empezar con la creacion de tu personaje" + "\n" + "Primero escribe tu nombre");
        String nombrecito = mover.nextLine();
        System.out.println("=======================================================================");
        System.out.println("Ahora elige tu clase" + "\n" + "Opcion 1 : Mago" + "\n" + "Opcion 2 : Guerrero");
        System.out.println("=======================================================================");
        //Aca crea el personaje segun la clase que selecciono
        do {
            opcion = errores.entrada(mover, opcion);
            switch (opcion) {
                case 1:
                    personaje = new Mago(nombrecito);
                    personaje.setTipo("mago");
                    break;
                case 2:
                    personaje = new Guerrero(nombrecito);
                    personaje.setTipo("guerrero");
                    break;
                default:
                    System.out.println("Opcion incorrecta o invalida!");
                    break;
            }
        } while (opcion > 2 || opcion <= 0);
        //Reseteo la opcion para poder seguir usandola mas abajo...
        opcion = 0;
        //Introduccion al personaje...
        System.out.println("=======================================================================");
        System.out.println("Bienvenido " + personaje.getTipoPersonaje() + " listo para matar enemigos?");
        System.out.println("=======================================================================");
        return personaje;
    }

    /* ==================================================================================================================================== */
    /* ====================================================== BATALLA Y SUS OPCIONES ====================================================== */
    /* ==================================================================================================================================== */
    public void batalla(Scanner mover, Entrada errores, List<Enemigo> enemigos, int totalEnemigos, Personaje personaje, HashMap<Integer, Lugar> lugares) {
        do {
            //Opciones de batalla correr o atacar!
            opcion = errores.entrada(mover, opcion);
            //Si la opcion es 1 empieza el combate y no termina hasta que la vida del enemigo sea menor o igual a 0
            switch (opcion) {
                case 1:
                    System.out.println(enemigos.get(dondeEstoy).getNombre() + " te ataca " + "\n" + " Narrador: " + enemigos.get(dondeEstoy).getIntroduccion() + "\n" + " Su vida actual es " + enemigos.get(dondeEstoy).getVida());

                    while (enemigos.get(dondeEstoy).getVida() > 0 && personaje.getVida() > 0) {
                        System.out.println("=======================================================================");
                        personaje.menuCombate(enemigos.get(dondeEstoy));
                        System.out.println("=======================================================================");
                        if (enemigos.get(dondeEstoy).getVida() > 0) {
                            enemigos.get(dondeEstoy).atacar(personaje, enemigos, dondeEstoy);
                        }
                    }   //Si el enemigo muere este if y si no el que esta fuera del loop por vida 0
                    if (enemigos.get(dondeEstoy).getVida() <= 0) {
                        totalEnemigos--;
                        System.out.println("OVERKILLLLLL");
                        System.out.println("=======================================================================");
                        System.out.println("Enemigos restantes para terminar el juego " + totalEnemigos);
                        System.out.println("=======================================================================");
                        System.out.println("Sigamos " + "\n" + " Narrador: Ahora estamos en " + lugares.get(dondeEstoy).toString() + " hacia donde vamos?");
                        opcion = 2;
                    } else if (personaje.getVida() <= 0) {
                        System.out.println(" TE MORISTE ENSERIO?! ");
                        System.exit(0);
                    }
                    break;
                case 2:
                    System.out.println("Narrador: Corramos! esperate! ¿HACIA DONDE?");
                    break;
                default:
                    enemigos.get(dondeEstoy).atacar(personaje, enemigos, dondeEstoy);
                    System.out.println("Esa no es una opcion AAAAAAAAAAHHHHHHHHH nos atacan");
                    break;
            }
        } while (opcion != 2);
    }

    /* ======================================================================================================================================================== */
    /* ================================================================ MOVIMIENTOS EN EL MAPA ================================================================ */
    /* ======================================================================================================================================================== */
    public void movimientos(Scanner mover, HashMap<Integer, Lugar> lugares, int totalEnemigos, Personaje personaje) {
        if (totalEnemigos != 0) {
            //Con este metodo voy mostrando las opciones segun el lugar en el que me encuentro...
            lugares.get(dondeEstoy).mostrarLugar(aDondeVoy, dondeEstoy, lugares);
            try {
                aDondeVoy = mover.nextInt();
            } catch (java.util.InputMismatchException e) {
                System.out.println("Error en el tipo de dato");
                mover.next();
                aDondeVoy = 0;
            }
            System.out.println("=======================================================================");
            System.out.println("Tu energia actual es " + personaje.getEnergia());
            System.out.println("=======================================================================");
            //Esto me va cambiando los lugares a los que voy segun las opciones que el usuario ingresa por consola...
            if (aDondeVoy >= 1 || aDondeVoy <= 4) {
                if (aDondeVoy == 1 && lugares.get(dondeEstoy).getHaciaElNorte() != 0) {
                    dondeEstoy = lugares.get(dondeEstoy).getHaciaElNorte();
                } else if (aDondeVoy == 2 && lugares.get(dondeEstoy).getHaciaElSur() != 0) {
                    dondeEstoy = lugares.get(dondeEstoy).getHaciaElSur();
                } else if (aDondeVoy == 3 && lugares.get(dondeEstoy).getHaciaElEste() != 0) {
                    dondeEstoy = lugares.get(dondeEstoy).getHaciaElEste();
                } else if (aDondeVoy == 4 && lugares.get(dondeEstoy).getHaciaElOeste() != 0) {
                    dondeEstoy = lugares.get(dondeEstoy).getHaciaElOeste();
                } else {
                    System.out.println("=======================================================================");
                    System.out.println("¡No hay nada hacia alli!");
                    System.out.println("=======================================================================");
                    //Si el personaje se queda quieto la energia se mantiene
                    personaje.setEnergia(personaje.getEnergia() + 1);
                }
                //Si el personaje camina entre lugares la energia desciende
                personaje.setEnergia(personaje.getEnergia() - 1);
            }
        }
    }

    /* ============================================================================================================================================================= */
    /* ================================================================ COMBATE CON EL ENEMIGO FINAL =============================================================== */
    /* ============================================================================================================================================================= */
    public void enemigoFinalcito(List<Enemigo> enemigos, Enemigo enemigo, Personaje personaje) {
        System.out.println("¡Una energia extraña te teletransporta hacia un lugar oscuro!");
        System.out.println(enemigos.get(13).getNombre() + "  " + enemigos.get(13).getIntroduccion());
        do {
            enemigo.atacar(personaje, enemigos, 13);
            personaje.menuCombate(enemigos.get(13));
        } while (personaje.getVida() > 0 && enemigos.get(13).getVida() > 0);
        if (personaje.getVida() > 0) {
            System.out.println("Narrador casi muertito: LO LOGRASTE " + personaje.getTipoPersonaje() + " me liberaste de seguirte a todos lados! te lo agradezco *se muere*" + "\n" + "Ganasteeee!!!!....");
        } else if (enemigos.get(13).getVida() > 0) {
            System.out.println("Narrador: yyy no se pudo chango...la proxima sera!" + "\n" + "Perdisteeee....");
        }
    }

    /* ============================================================================================================================================================= */
    /* ====================================================== FINAL DEL JUEGO CON EL COMBATE ANTES MENCIONADO ====================================================== */
    /* ============================================================================================================================================================= */
    public void finalJuego(int totalEnemigos, Personaje personaje, List<Enemigo> enemigos, Enemigo enemigo) {
        if (totalEnemigos <= 0) {
            System.out.println("=======================================================================");
            System.out.println("Narrador: ESPERATE... ¡GANASTE! " + personaje.getTipoPersonaje() + " YA NO HAY MAS MONSTRUOS ACECHANDO EN LA OSCURIDAD cierto???");
            System.out.println("=======================================================================");
            System.out.println("*pausa dramatica*");
            try {
                Thread.sleep(3000);
                System.out.print(" . ");
                Thread.sleep(3000);
                System.out.print(" . ");
                Thread.sleep(3000);
                System.out.print(" . ");
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                System.out.println("Excepcion encontrada " + ex.getMessage());
            }
            System.out.println("=======================================================================");
            enemigoFinalcito(enemigos, enemigo, personaje);
            System.out.println("=======================================================================");
        } else if (personaje.getEnergia() <= 0) {
            System.out.println("Tu personaje se quedo sin fuentes energia para correr o pelear hasta aca llega tu batalla vaquero/a " + personaje.getTipoPersonaje());
        }
    }
}
