package sistemas;

import java.util.Scanner;

public class Entrada {

    //Este metodo me permite evitar las excepciones cuando ingreso algo por la consola y no es un numero...
    public int entrada(Scanner algo, int opcion) {
        while (!algo.hasNextInt()) {
            System.out.println("Necesito un numero");
            algo.nextLine();
        }
        opcion = algo.nextInt();
        return opcion;
    }
}
